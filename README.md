ics-ans-role-zfs-install
===================

Ansible role to install zfs on linux

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
None
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zfs-install
```

License
-------

BSD 2-clause
